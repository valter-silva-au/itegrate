package com.planit.itegrate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItegrateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItegrateApplication.class, args);
	}
}
