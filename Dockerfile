FROM openjdk:8-jdk

RUN groupadd --system builder \
    && useradd --system builder --gid builder --shell /usr/sbin/nologin

COPY --chown=builder:builder . /opt/itegrate/

WORKDIR /opt/itegrate/

RUN ./gradlew build